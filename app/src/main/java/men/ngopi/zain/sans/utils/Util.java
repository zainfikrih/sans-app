package men.ngopi.zain.sans.utils;

import android.content.Context;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.function.Consumer;

public class Util {

    public static void showToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static String formatDate(String dateStr){
        Date date = null;
        try {
            DateFormat dateFormat = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss", Locale.US);
            date = dateFormat.parse(dateStr);

            DateFormat dateFormat1 = new SimpleDateFormat("HH:mm", Locale.US);
            String dateFormatStr = dateFormat1.format(date);

            return dateFormatStr;
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return "";
    }
}
package men.ngopi.zain.sans.features.main.order;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.victor.loading.rotate.RotateLoading;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.controllers.OrderController;
import men.ngopi.zain.sans.features.restaurantmenu.MenuFragment;
import men.ngopi.zain.sans.models.Category;
import men.ngopi.zain.sans.models.Order;
import men.ngopi.zain.sans.models.Restaurant;
import men.ngopi.zain.sans.network.Api;

public class OrdersFragment extends Fragment {

    private View view;
    static private OrdersFragment ordersFragment;
    private ViewPager viewPager;
    private Bundle bundle;
    private SmartTabLayout viewPagerTab;
    private List<String> tabItems = new ArrayList<>();
    private OrderFragmentAdapter orderFragmentAdapter;
    private ArrayList<Order> process = new ArrayList<>();
    private ArrayList<Order> complete = new ArrayList<>();
    private ArrayList<Order> orders;
    private ArrayList<Order> orderHistory = new ArrayList<>();
    private int counterTab = 0;

    private RotateLoading rotateLoading;

    static public OrdersFragment newInstance(){
        ordersFragment = new OrdersFragment();
        return ordersFragment;
    }

    public static OrdersFragment getInstance(){
        return ordersFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_orders, container, false);

        Rak.initialize(getContext());

        Log.i("ID TOKEN", Rak.grab(Api.ID_TOKEN, ""));

//        CustomerController.getInstance(getActivity()).getCustomer();

        viewPager = (ViewPager) view.findViewById(R.id.orders_vp);
        viewPager.setOffscreenPageLimit(10);
        viewPagerTab = (SmartTabLayout) view.findViewById(R.id.orders_tab);
        rotateLoading = view.findViewById(R.id.rotateloadingOrders);
        rotateLoading.start();

        orderFragmentAdapter = new OrderFragmentAdapter(getChildFragmentManager());
        viewPager.setAdapter(orderFragmentAdapter);

//        if(Rak.isExist("orderHistory")){
//            orderHistory = Rak.grab("orderHistory");
//            showOrderHistory(orderHistory);
//        }

        OrderController.getInstance(getActivity()).getOrderHistory();

        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

//    public void showOrderHistory(ArrayList<Order> orders){
//        try {
//            Log.e("ERROR DISNIN", "Hai");
//            for(Order order: orders){
//                if(order.getStatus().equalsIgnoreCase("process")){
//                    process.add(order);
//                }
//                if(order.getStatus().equalsIgnoreCase("complete")){
//                    complete.add(order);
//                }
//            }
//            viewPagerTab.setViewPager(viewPager);
//        } catch (Exception e){
//
//        }
//    }

    public void showOrderHistory(ArrayList<Order> process, ArrayList<Order> complete){
        try {
            this.process = process;
            this.complete = complete;
            if (counterTab == 0){
                addTabPage("Process");
                addTabPage("Complete");
                counterTab++;
            }
            viewPagerTab.setViewPager(viewPager);
        } catch (Exception e){

        }
    }

    public void addTabPage(String title) {
        try {
            tabItems.add(title);
            orderFragmentAdapter.notifyDataSetChanged();
            rotateLoading.stop();
        } catch (Exception e){

        }
    }

    private class OrderFragmentAdapter extends FragmentPagerAdapter {

        public OrderFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int i) {
            bundle = new Bundle();
            Type listType = new TypeToken<ArrayList<Order>>() {}.getType();
            String ordersStr = "";
            Log.d("IIII", String.valueOf(i));
            if(i==0){
                ordersStr = new Gson().toJson(process, listType);
            } else {
                ordersStr = new Gson().toJson(complete, listType);
            }
            Log.d("ORDER STR", ordersStr);
            bundle.putString("orders", ordersStr);
            return OrderFragment.newInstance(bundle);
        }

        @Override
        public int getCount() {
            return tabItems.size();
        }

        @android.support.annotation.Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabItems.get(position);
        }
    }
}

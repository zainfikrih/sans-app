package men.ngopi.zain.sans.features.recomendedrestaurant;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

import java.util.ArrayList;
import java.util.List;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.RestaurantController;
import men.ngopi.zain.sans.features.restaurantmenu.MenuFragment;
import men.ngopi.zain.sans.features.restaurantmenu.RestaurantActivity;
import men.ngopi.zain.sans.models.Category;
import men.ngopi.zain.sans.models.Restaurant;
import men.ngopi.zain.sans.server.Server;
import men.ngopi.zain.sans.utils.Util;

public class RecomendedRestaurantActivity extends AppCompatActivity {

    private static RecomendedRestaurantActivity recomendedRestaurantActivity;

    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView imageView;

    private Restaurant restaurantInfo;
    private String idRestaurant;
    private String openingTime;
    private String closingTime;

    private TextView tvLocation, tvTime;

    private MenuFragmentAdapter adapterPage;
    private List<String> tabItems = new ArrayList<>();
    private Bundle bundle;
    private ViewPager viewPager;
    private SmartTabLayout viewPagerTab;

    public static RecomendedRestaurantActivity getInstance(){
        return recomendedRestaurantActivity;
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recomended_restaurant);
        Rak.initialize(getApplicationContext());

        recomendedRestaurantActivity = this;

        restaurantInfo = Rak.grab("restaurantRecInfo");

        collapsingToolbarLayout = findViewById(R.id.restaurant_info_collapsing_toolbar);
        imageView = findViewById(R.id.restaurant_info_img_toolbar);
        toolbar = findViewById(R.id.restaurant_info_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(restaurantInfo.getName());
        getSupportActionBar().setSubtitle(restaurantInfo.getAddress());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable drawable = getDrawable(R.drawable.ic_info);
        toolbar.setOverflowIcon(drawable);

        openingTime = Util.formatDate(restaurantInfo.getOpening_time());
        closingTime = Util.formatDate(restaurantInfo.getClosing_time());

        String url = "https://api.sans.ngopi.men"+restaurantInfo.getPicture();

        Glide.with(this)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_room_service)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        imageView.setImageDrawable(resource);

                        Bitmap bm = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
                        Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
                            @Override
                            public void onGenerated(Palette palette) {
                                int mutedColor = palette.getDarkMutedColor(R.attr.colorPrimary); //Muted, Dark, Light, Vibrant
                                collapsingToolbarLayout.setContentScrimColor(mutedColor);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    Window window = getWindow();
                                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                    window.setStatusBarColor(mutedColor);
                                    int flags = window.getDecorView().getSystemUiVisibility();
                                    flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                                    window.getDecorView().setSystemUiVisibility(flags);
                                }
                            }
                        });
                    }
                });

        showRestaurant(restaurantInfo);

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu_restoran, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.options_menu_restoran:
                // setup the alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(RecomendedRestaurantActivity.this, R.style.AppCompatAlertDialogStyle);
                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_recomended_restaurant, null);

                tvLocation = dialogView.findViewById(R.id.dialog_restaurant_info_location);
                tvTime = dialogView.findViewById(R.id.dialog_restaurant_info_time);

                tvLocation.setText(restaurantInfo.getAddress());
                String time = openingTime + " - " + closingTime;
                tvTime.setText(time);

                builder.setView(dialogView);
                builder.setTitle(restaurantInfo.getName());

                // add the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addTab(String title) {
//        viewPagerTab.addTab(viewPagerTab.newTab().setText(title));
        addTabPage(title);
    }

    public void addTabPage(String title) {
        tabItems.add(title);
        adapterPage.notifyDataSetChanged();
    }

    public void showRestaurant(Restaurant restaurant){
//        this.restaurantInfo = restaurant;
//        Toast.makeText(RecomendedRestaurantActivity.this, restaurant.getName(), Toast.LENGTH_SHORT).show();

        List<Category> categories = restaurant.getCategories();

        List<String> tabName = new ArrayList<>();
        for (Category category: categories) {
            tabName.add(category.getName());
        }

        viewPager = (ViewPager) findViewById(R.id.restaurant_info_vp);
        viewPager.setOffscreenPageLimit(10);
        viewPagerTab = (SmartTabLayout) findViewById(R.id.restaurant_info_tab);

        adapterPage = new MenuFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterPage);

        for(int i = 0; i < tabName.size(); i++){
            addTab(tabName.get(i));
        }

        viewPagerTab.setViewPager(viewPager);
    }

    private class MenuFragmentAdapter extends FragmentPagerAdapter {

        public MenuFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            bundle = new Bundle();
//            bundle.putStringArrayList("strList", list.get(i));
//            Rak.entry("menus", restaurant.getCategories().get(i).getMenus());
//            bundle.putParcelableArrayList("menus", restaurant.getCategories().get(i).getMenus());
            Gson gson = new GsonBuilder().create();
            String menus = gson.toJson(restaurantInfo.getCategories().get(i).getMenus());
            bundle.putString("menus", menus);
            return RecMenuFragment.newInstance(bundle);
        }

        @Override
        public int getCount() {
            return tabItems.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabItems.get(position);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

}

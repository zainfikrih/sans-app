package men.ngopi.zain.sans.features.main.favorite;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.victor.loading.rotate.RotateLoading;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.controllers.OrderController;
import men.ngopi.zain.sans.features.main.favorite.adapters.FavoriteAdapter;
import men.ngopi.zain.sans.features.main.order.OrderFragment;
import men.ngopi.zain.sans.features.main.order.adapters.OrderAdapter;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.models.Order;
import men.ngopi.zain.sans.network.Api;

public class FavoriteFragment extends Fragment {

    private View view;
    static private FavoriteFragment favoriteFragment;
    private ArrayList<Menu> menus;

    private RecyclerView recyclerView;
    private RotateLoading rotateLoading;

    static public FavoriteFragment newInstance(){
        favoriteFragment = new FavoriteFragment();
        return favoriteFragment;
    }

    public static FavoriteFragment getInstance(){
        return favoriteFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorite, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Rak.initialize(getContext());

        recyclerView = view.findViewById(R.id.favorite_recycler_view);
        rotateLoading = view.findViewById(R.id.rotateloadingFavorite);
        rotateLoading.start();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        if(Rak.isExist("favoriteMenus")){
            menus = Rak.grab("favoriteMenus");
            showFavoriteMenu(menus);
        }

        CustomerController.getInstance(getActivity()).getFavotiteMenu();
    }

    public void showFavoriteMenu(ArrayList<Menu> menus){
        try {
            FavoriteAdapter favoriteAdapter = new FavoriteAdapter(getActivity(), menus);
            recyclerView.setAdapter(favoriteAdapter);
            rotateLoading.stop();
        } catch (Exception e){

        }

    }
}

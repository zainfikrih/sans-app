package men.ngopi.zain.sans.models;

import java.util.ArrayList;

public class Order {
    private String id;
    private int total_price;
    private int table_number;
    private String order_number;
    private String status;
    private Restaurant restaurant;

    private String order_items;
    private String payment_method;
    private String customer_id;

    public Order(){

    }

    public Order(String id, int total_price, int table_number, String order_number, String status, Restaurant restaurant, String order_items, String payment_method, String customer_id) {
        this.id = id;
        this.total_price = total_price;
        this.table_number = table_number;
        this.order_number = order_number;
        this.status = status;
        this.restaurant = restaurant;
        this.order_items = order_items;
        this.payment_method = payment_method;
        this.customer_id = customer_id;
    }

    public String getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(String customer_id) {
        this.customer_id = customer_id;
    }

    public String getPayment_method() {
        return payment_method;
    }

    public void setPayment_method(String payment_method) {
        this.payment_method = payment_method;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getTotal_price() {
        return total_price;
    }

    public void setTotal_price(int total_price) {
        this.total_price = total_price;
    }

    public int getTable_number() {
        return table_number;
    }

    public void setTable_number(int table_number) {
        this.table_number = table_number;
    }

    public String getOrder_number() {
        return order_number;
    }

    public void setOrder_number(String order_number) {
        this.order_number = order_number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }

    public String getOrder_items() {
        return order_items;
    }

    public void setOrder_items(String order_items) {
        this.order_items = order_items;
    }
}

package men.ngopi.zain.sans.features.main.order;

import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.features.main.MainActivity;
import men.ngopi.zain.sans.features.main.order.adapters.OrderAdapter;
import men.ngopi.zain.sans.features.restaurantmenu.adapters.MenuAdapter;
import men.ngopi.zain.sans.models.Order;
import men.ngopi.zain.sans.network.Api;

public class OrderFragment extends Fragment {
    private View view;
    static private OrderFragment orderFragment;
    private RecyclerView recyclerView;

    static public OrderFragment newInstance(Bundle bundle){
        orderFragment = new OrderFragment();
        orderFragment.setArguments(bundle);
        return orderFragment;
    }

    public static OrderFragment getInstance(){
        return orderFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_order, container, false);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Rak.initialize(getContext());

        recyclerView = view.findViewById(R.id.order_recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity().getParent()));
        recyclerView.setHasFixedSize(true);

        try {
            String orderStr = getArguments().getString("orders");

            if(!orderStr.isEmpty()){
                Log.d("ISI ORDER STR", orderStr);
                Type listType = new TypeToken<ArrayList<Order>>() {}.getType();
                ArrayList<Order> order = new Gson().fromJson(orderStr, listType);

                if(!order.isEmpty()){
                    OrderAdapter orderAdapter = new OrderAdapter(getActivity(), order);
                    recyclerView.setAdapter(orderAdapter);
                    orderAdapter.notifyDataSetChanged();
                }

            }
        } catch (Exception e){

        }

    }
}

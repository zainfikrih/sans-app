package men.ngopi.zain.sans.controllers;

import android.content.Context;

import java.util.ArrayList;

import men.ngopi.zain.sans.features.main.favorite.FavoriteFragment;
import men.ngopi.zain.sans.features.main.more.MoreFragment;
import men.ngopi.zain.sans.models.Customer;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.server.Server;

public class CustomerController {

    private static CustomerController customerController;
    private static Context context;

    public static CustomerController getInstance(Context cntx){
        if(customerController == null){
            customerController = new CustomerController();
        }
        context = cntx;
        return customerController;
    }

    public void getCustomer(){
        Server.getInstance(context).getCustomer();
    }

    public void showCustomer(Customer object){
//        MoreFragment.getInstance().showCustomer(object);
    }

    public void addFavoriteMenu(String menuId){
        Server.getInstance(context).addFavoriteMenu(menuId);
    }

    public void successAddFavoriteMenu(){

    }

    public void unFavoriteMenu(String menuId){
        Server.getInstance(context).unFavoriteMenu(menuId);
    }

    public void successUnFavoriteMenu(){

    }

    public void getFavotiteMenu(){
        Server.getInstance(context).favoriteMenu();
    }

    public void successGetFavoriteMenu(ArrayList<Menu> menus){
        FavoriteFragment.getInstance().showFavoriteMenu(menus);
    }
}
package men.ngopi.zain.sans.features.recomendedrestaurant.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.button.MaterialButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.controllers.RestaurantController;
import men.ngopi.zain.sans.features.restaurantmenu.RestaurantActivity;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.models.OrderItem;
import men.ngopi.zain.sans.models.Restaurant;
import men.ngopi.zain.sans.server.Server;

public class RecMenuAdapter extends RecyclerView.Adapter<RecMenuAdapter.MenuViewHolder> {

    private Context context;
    private ArrayList<Menu> menu;
    private LayoutInflater layoutInflater;
    private View view;

    private ArrayList<OrderItem> orderItems = new ArrayList<>();
    private HashMap<String, OrderItem> orderItemHashMap = new HashMap<>();
    private HashMap<String, Menu> menuHashMap = new HashMap<>();

    public RecMenuAdapter(Context context, ArrayList<Menu> menu){
        this.context = context;
        this.menu = menu;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public RecMenuAdapter.MenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = layoutInflater.inflate(R.layout.menu_item, viewGroup, false);
        return new RecMenuAdapter.MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecMenuAdapter.MenuViewHolder menuViewHolder, final int i) {
        final Menu menuItem = menu.get(i);
        menuViewHolder.tvName.setText(menuItem.getName());
        menuViewHolder.tvDesc.setText(menuItem.getDescription());
        menuViewHolder.tvPrice.setText(String.valueOf("Rp. "+menuItem.getPrice()));
        if(menuItem.getLiked()){
            menuViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_true));
        }

        String url = "https://api.sans.ngopi.men"+menuItem.getImage();

        Glide.with(context)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_room_service)
                .into(menuViewHolder.ivMenu);

        menuViewHolder.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(menuItem.getLiked()){
                    CustomerController.getInstance(context.getApplicationContext()).unFavoriteMenu(String.valueOf(menuItem.getId()));
                    menuViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite));
                    menuViewHolder.ivFav.setColorFilter(ContextCompat.getColor(context, R.color.colorGrayBtn), android.graphics.PorterDuff.Mode.MULTIPLY);
                    menu.get(i).setLiked(false);
                } else {
                    CustomerController.getInstance(context.getApplicationContext()).addFavoriteMenu(String.valueOf(menuItem.getId()));
                    menuViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_true));
                    menu.get(i).setLiked(true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvDesc;
        private TextView tvPrice;
        private TextView tvTotal;
        private MaterialButton btnPlus;
        private MaterialButton btnMin;
        private ImageView ivMenu;
        private ImageView ivFav;
        private ImageView ivNote;

        public MenuViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.menu_tittle);
            tvDesc = itemView.findViewById(R.id.menu_desc);
            tvPrice = itemView.findViewById(R.id.menu_price);
            tvTotal = itemView.findViewById(R.id.menu_total);
            btnPlus = itemView.findViewById(R.id.menu_btn_plus);
            btnMin = itemView.findViewById(R.id.menu_btn_minus);
            ivMenu = itemView.findViewById(R.id.menu_img);
            ivFav = itemView.findViewById(R.id.menu_fav);
            ivNote = itemView.findViewById(R.id.menu_note);

            btnPlus.setVisibility(View.GONE);
            btnMin.setVisibility(View.GONE);
            ivNote.setVisibility(View.GONE);
            tvTotal.setVisibility(View.GONE);

        }
    }
}

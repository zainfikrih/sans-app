package men.ngopi.zain.sans.features.main.order.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.button.MaterialButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.features.restaurantmenu.RestaurantActivity;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.models.Order;
import men.ngopi.zain.sans.models.OrderItem;

public class OrderAdapter extends RecyclerView.Adapter<OrderAdapter.OrderViewHolder> {

    private Context context;
    private ArrayList<Order> orders;
    private LayoutInflater layoutInflater;
    private View view;

    public OrderAdapter(Context context, ArrayList<Order> orders){
        this.context = context;
        this.orders = orders;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public OrderAdapter.OrderViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = layoutInflater.inflate(R.layout.order_items, viewGroup, false);
        return new OrderAdapter.OrderViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderAdapter.OrderViewHolder orderViewHolder, final int i) {
        final Order menuItem = orders.get(i);

        orderViewHolder.tvName.setText(menuItem.getRestaurant().getName());
        orderViewHolder.tvId.setText("Orde Id: " + menuItem.getId());
        orderViewHolder.tvPrice.setText(String.valueOf("Rp. " + menuItem.getTotal_price()));

        String url = "https://api.sans.ngopi.men"+menuItem.getRestaurant().getPicture();

        Glide.with(context)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_room_service)
                .into(orderViewHolder.ivRestaurant);
    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    public class OrderViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvId;
        private TextView tvPrice;
        private ImageView ivRestaurant;

        public OrderViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.orderItems_name);
            tvId = itemView.findViewById(R.id.orderItems_id);
            tvPrice = itemView.findViewById(R.id.orderItems_price);
            ivRestaurant = itemView.findViewById(R.id.order_items_img);

        }
    }
}

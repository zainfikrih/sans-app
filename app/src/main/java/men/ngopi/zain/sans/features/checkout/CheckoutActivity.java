package men.ngopi.zain.sans.features.checkout;

import android.annotation.TargetApi;
import android.graphics.Color;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.controllers.OrderController;
import men.ngopi.zain.sans.features.checkout.adapters.OrderItemAdapter;
import men.ngopi.zain.sans.features.restaurantmenu.RestaurantActivity;
import men.ngopi.zain.sans.features.restaurantmenu.adapters.MenuAdapter;
import men.ngopi.zain.sans.models.Customer;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.models.Order;
import men.ngopi.zain.sans.models.OrderItem;
import men.ngopi.zain.sans.models.Restaurant;
import men.ngopi.zain.sans.network.Api;
import men.ngopi.zain.sans.utils.JSParser;

public class CheckoutActivity extends AppCompatActivity {

    private static CheckoutActivity checkoutActivity;
    private Toolbar toolbar;
    private TextView tvName;
    private TextView tvAddress;
    private TextView tvTable;
    private TextView tvPrice;
    private Button btnAddMenu;
    private Button btnOrder;
    private RecyclerView recyclerView;

    private Restaurant restaurant;
    private String tableNumber;
    private String orderQuery;
    private Customer customer;

    private ArrayList<OrderItem> orderItems = new ArrayList<>();
    private ArrayList<Menu> menuItems = new ArrayList<>();
    private HashMap<String, OrderItem> orderItemHashMap = new HashMap<>();
    private HashMap<String, Menu> menuHashMap = new HashMap<>();

    private int totalPrice = 0;
    private SweetAlertDialog loadingDialog;

    public static CheckoutActivity getInstance(){
        return checkoutActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        CustomerController.getInstance(getApplicationContext()).getCustomer();
        AndroidNetworking.initialize(getApplicationContext());
        Rak.initialize(getApplicationContext());
        checkoutActivity = this;

        customer = Rak.grab("customer");
        restaurant = Rak.grab("restaurant");
        tableNumber = Rak.grab("table");
        orderItemHashMap = Rak.grab("orderItems");
        menuHashMap = Rak.grab("menuItems");


        toolbar = findViewById(R.id.checkout_toolbar);
        tvName = findViewById(R.id.checkout_restaurant);
        tvAddress = findViewById(R.id.checkout_address);
        tvTable = findViewById(R.id.checkout_table);
        tvPrice = findViewById(R.id.checkout_price);
        btnAddMenu = findViewById(R.id.checkout_btn_add_menu);
        btnOrder = findViewById(R.id.checkout_btn_order);

        tvName.setText(restaurant.getName());
        tvAddress.setText(restaurant.getAddress());
        tvTable.setText(tableNumber);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        btnAddMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Balance", String.valueOf(customer.getPayment().getBalance()));
                if(customer.getPayment().getBalance() >= totalPrice){
                    // Loading dialog
                    loadingDialog = new SweetAlertDialog(CheckoutActivity.this, SweetAlertDialog.PROGRESS_TYPE);
                    loadingDialog.getProgressHelper().setBarColor(Color.parseColor("#225498"));
                    loadingDialog.setTitleText("Ordering");
                    loadingDialog.setCancelable(false);

                    Order order = new Order();
                    order.setCustomer_id(customer.getId());
                    order.setRestaurant(restaurant);
                    order.setOrder_items(orderQuery);
                    order.setTable_number(Integer.valueOf(tableNumber));

                    loadingDialog.show();
                    OrderController.getInstance(getApplicationContext()).createOrder(order);
                } else {
                    errorCreateOrder();
                }
            }
        });

        recyclerView = findViewById(R.id.rv_order_items);

        recyclerView.setLayoutManager(new LinearLayoutManager(CheckoutActivity.this));
        recyclerView.setHasFixedSize(true);

        orderItems.addAll(orderItemHashMap.values());
        menuItems.addAll(menuHashMap.values());

        OrderItemAdapter orderItemAdapter = new OrderItemAdapter(CheckoutActivity.this, orderItems, menuHashMap);

        recyclerView.setAdapter(orderItemAdapter);

        // Price
        String price = getIntent().getStringExtra("calculatedPrice");
        calculatePrice(price);

        // Calculate Server
//        calculateTotalPrice();
    }

    @Override
    protected void onStart() {
        super.onStart();
        customer = Rak.grab("customer");
        Log.d("Balance", String.valueOf(customer.getPayment().getBalance()));
    }

    public void successCreateOrder(){
        loadingDialog.dismiss();
        RestaurantActivity.getInstance().finish();
        finish();
    }

    public void errorCreateOrder(){
        loadingDialog = new SweetAlertDialog(CheckoutActivity.this, SweetAlertDialog.ERROR_TYPE);
        loadingDialog.setTitleText("Oops...");
        loadingDialog.setContentText("Your balance is not enough");
        loadingDialog.setCancelable(true);
        loadingDialog.show();
    }

    public void calculatePrice(String price){
        String order = new Gson().toJson(orderItems);
        String order2 = order.replaceAll("\"menu_id\"", "menu_id");
        String order3 = order2.replaceAll("\"note\"", "note");
        orderQuery = order3.replaceAll("\"quantity\"", "quantity");

        totalPrice = Integer.valueOf(price);
        tvPrice.setText(price);
        CustomerController.getInstance(getApplicationContext()).getCustomer();
    }

    @TargetApi(Build.VERSION_CODES.N)
    public void calculateTotalPrice(){
//        String orderQuery = JSParser.parseArray(orderItemsObject, true);
        String order = new Gson().toJson(orderItems);
        String order2 = order.replaceAll("\"menu_id\"", "menu_id");
        String order3 = order2.replaceAll("\"note\"", "note");
        orderQuery = order3.replaceAll("\"quantity\"", "quantity");
        Log.d("Array Order", orderQuery);
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "\n" +
                            "mutation{\n" +
                            "  calculateTotalPrice(order_items: "+ orderQuery +"){\n" +
                            "    total_price\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("unFavoriteMenu")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data").getJSONObject("calculateTotalPrice");
                                totalPrice = jsonObject.getInt("total_price");
                                tvPrice.setText(String.valueOf(jsonObject.getInt("total_price")));
                                CustomerController.getInstance(getApplicationContext()).getCustomer();
//                                CustomerController.getInstance(context).showCustomer(customer);
                                //CustomerController.getInstance().showCustomer(response.getJSONObject("data").getJSONObject("customer"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }
}


package men.ngopi.zain.sans.utils;

import android.os.Build;

import androidx.annotation.RequiresApi;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.function.Consumer;

public class JSONParser {
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String parseObject(Object obj, Boolean usingNull)  {
        HashMap<String, Object> res = new HashMap<>();
        String result = "{ ";
        for (Field field : obj.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            String name = field.getName();
            Object value = null;
            try {
                value = field.get(obj);
            } catch (IllegalAccessException e) {

            }

            res.put(name, value);

            if (usingNull) if (value == null) {
                result += "\"" + name + "\": " + "null, ";
            }

            if (value instanceof Integer) {
                result += "\"" + name + "\": " + value + ", ";
            } else if (value instanceof String) {
                result += "\"" + name + "\": \"" + value + "\", ";
            } else if (value != null) {
                if (value instanceof ArrayList) {
                    result += "\"" + name + "\": " + parseArray((ArrayList) value, usingNull) + ", ";
                } else {
                    result += "\"" + name + "\": " + parseObject(value, usingNull) + ", ";
                }
            }


//            System.out.printf("%s: %s%n", name, value);
        }

        if (result.length() > 2) {
            result = result.substring(0, result.length() - 2);
        }

        return result + " }";
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String parseObject(Object obj) {
        return parseObject(obj, false);
    }

    public static String parseArrayResult;
    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String parseArray(ArrayList<Object> objs, final Boolean usingNull) {
        parseArrayResult = "[ ";
        objs.forEach(new Consumer<Object>() {
            @Override
            public void accept(Object obj) {
                parseArrayResult += parseObject(obj, usingNull) + ", ";
            }
        });
        if (parseArrayResult.length() > 2) {
            parseArrayResult = parseArrayResult.substring(0, parseArrayResult.length() - 2);
        }
        return parseArrayResult + " ]";
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String parseArray(ArrayList<Object> objs) {
        return parseArray(objs, false);
    }
}

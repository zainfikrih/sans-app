package men.ngopi.zain.sans.models;

import java.util.ArrayList;
import java.util.List;

public class Category {
    private int id;
    private String name;
    private ArrayList<Menu> menus;

    public Category(int id, String name, ArrayList<Menu> menus) {
        this.id = id;
        this.name = name;
        this.menus = menus;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(ArrayList<Menu> menus) {
        this.menus = menus;
    }
}

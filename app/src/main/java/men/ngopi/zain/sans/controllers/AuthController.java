package men.ngopi.zain.sans.controllers;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.GoogleAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.Executor;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.features.login.LoginActivity;
import men.ngopi.zain.sans.features.main.MainActivity;
import men.ngopi.zain.sans.features.main.more.MoreFragment;
import men.ngopi.zain.sans.network.Api;
import men.ngopi.zain.sans.server.Server;

public class AuthController {

    private static AuthController authController;
    private static Context context;

    public static AuthController getInstance(Context cntx){
        if(authController == null){
            authController = new AuthController();
        }
        context = cntx;
        return authController;
    }

    public void firebaseAuthWithGoogle(GoogleSignInAccount acct, final FirebaseAuth mAuth, final Context context){
        Log.d("Firebase Auth Google : ", "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(LoginActivity.getInstance(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("Auth : ", "signInWithCredential:success");

                            FirebaseUser mUser;
                            mUser = mAuth.getCurrentUser();
                            mUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                @Override
                                public void onComplete(@NonNull Task<GetTokenResult> task) {
                                    String idToken = task.getResult().getToken();
                                    Log.i("ID TOKEN Firebase", idToken);

                                    Server.getInstance(context).customerLogin(idToken);

                                }
                            });
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("Auth : ", "signInWithCredential:failure", task.getException());
                            authFailed();
                        }
                    }
                });
    }

    public void authFailed(){
        LoginActivity.getInstance().authFailed();
    }

    public void authSuccess(){
        LoginActivity.getInstance().authSuccess();
    }

    public void logout(FirebaseAuth firebaseAuth, GoogleSignInClient mGoogleSignInClient){
        firebaseAuth.signOut();

        mGoogleSignInClient.signOut().addOnCompleteListener(MoreFragment.getInstance().getActivity(),new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                MoreFragment.getInstance().showLoginActivity();
            }
        });
    }
}

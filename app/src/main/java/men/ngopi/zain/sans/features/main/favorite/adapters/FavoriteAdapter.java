package men.ngopi.zain.sans.features.main.favorite.adapters;

import android.content.Context;
import android.graphics.PorterDuff;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.models.Order;

public class FavoriteAdapter extends RecyclerView.Adapter<FavoriteAdapter.FavoriteViewHolder> {

    private Context context;
    private ArrayList<Menu> menus;
    private LayoutInflater layoutInflater;
    private View view;

    public FavoriteAdapter(Context context, ArrayList<Menu> menus){
        this.context = context;
        this.menus = menus;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public FavoriteAdapter.FavoriteViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = layoutInflater.inflate(R.layout.favorite_item, viewGroup, false);
        return new FavoriteAdapter.FavoriteViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final FavoriteAdapter.FavoriteViewHolder favoriteViewHolder, final int i) {
        final Menu menuItem = menus.get(i);

        favoriteViewHolder.tvName.setText(menuItem.getName());
        favoriteViewHolder.tvDesc.setText(menuItem.getDescription());
        favoriteViewHolder.tvPrice.setText(String.valueOf("Rp. "+menuItem.getPrice()));
        if(menuItem.getLiked()){
            favoriteViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_true));
        }

        String url = "https://api.sans.ngopi.men"+menuItem.getImage();

        Glide.with(context)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_room_service)
                .into(favoriteViewHolder.ivImg);

        favoriteViewHolder.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(menuItem.getLiked()){
                    CustomerController.getInstance(context.getApplicationContext()).unFavoriteMenu(String.valueOf(menuItem.getId()));
                    favoriteViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite));
                    favoriteViewHolder.ivFav.setColorFilter(ContextCompat.getColor(context, R.color.colorGrayBtn), android.graphics.PorterDuff.Mode.MULTIPLY);
                    menuItem.setLiked(false);
                } else {
                    CustomerController.getInstance(context.getApplicationContext()).addFavoriteMenu(String.valueOf(menuItem.getId()));
                    favoriteViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_true));
                    menuItem.setLiked(true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return menus.size();
    }

    public class FavoriteViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvDesc;
        private TextView tvPrice;
        private ImageView ivFav;
        private ImageView ivImg;

        public FavoriteViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.fav_tittle);
            tvDesc = itemView.findViewById(R.id.fav_desc);
            tvPrice = itemView.findViewById(R.id.fav_price);
            ivFav = itemView.findViewById(R.id.fav_fav);
            ivImg = itemView.findViewById(R.id.fav_img);
        }
    }
}

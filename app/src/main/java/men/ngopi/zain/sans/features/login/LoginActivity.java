package men.ngopi.zain.sans.features.login;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.net.URL;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.AuthController;
import men.ngopi.zain.sans.features.main.MainActivity;
import men.ngopi.zain.sans.features.main.more.MoreFragment;

public class LoginActivity extends AppCompatActivity {

    private TextInputEditText txtNumberPhone;
    private Button btnSignIn, btnSignInGoogle;
    private static LoginActivity loginActivity;

    private GoogleSignInClient mGoogleSignInClient;
    private final static int RC_SIGN_IN = 123;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;

    private ProgressBar progressBar;

    public static LoginActivity getInstance(){
        return loginActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        loginActivity = this;

        txtNumberPhone = findViewById(R.id.txtPhoneNumber);
        btnSignIn = findViewById(R.id.btnSignIn);
        btnSignInGoogle = findViewById(R.id.btnSignInGoogle);
        progressBar = findViewById(R.id.progressbarLogin);
        progressBar.setVisibility(View.INVISIBLE);

        Rak.initialize(getApplicationContext());

        // Phone Auth
//        btnSignIn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if(txtNumberPhone.getText().toString().isEmpty()){
//                    Toast.makeText(LoginActivity.this, "Please insert phone number", Toast.LENGTH_SHORT).show();
//                } else {
//                    Intent verifyIntent = new Intent(LoginActivity.this, VerifyActivity.class);
//                    verifyIntent.putExtra("phoneNumber", txtNumberPhone.getText().toString());
//                    startActivity(verifyIntent);
//                }
//            }
//        });

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("537841684101-gcjou14igd5a1dar9qpufsqku8q16dmg.apps.googleusercontent.com")
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);
        mAuth = FirebaseAuth.getInstance();

        // Google Auth
        btnSignInGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                signIn();
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);

                //ProgressBar
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                        WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                progressBar.setVisibility(View.VISIBLE);

//                firebaseAuthWithGoogle(account);
                AuthController.getInstance(getApplicationContext()).firebaseAuthWithGoogle(account, mAuth, getApplicationContext());

            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w("Google Auth : ", "Google sign in failed", e);
                // ...
            }
        }
    }

    public void authSuccess(){
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }

    public void authFailed(){
        if(mUser != null){
            // Configure Google Sign In
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken("537841684101-gcjou14igd5a1dar9qpufsqku8q16dmg.apps.googleusercontent.com")
                    .requestEmail()
                    .build();
            mGoogleSignInClient = GoogleSignIn.getClient(LoginActivity.this, gso);

            mAuth.signOut();

            mGoogleSignInClient.signOut().addOnCompleteListener(MoreFragment.getInstance().getActivity(),new OnCompleteListener<Void>() {
                @Override
                public void onComplete(@NonNull Task<Void> task) {

                }
            });
        }
        progressBar.setVisibility(View.INVISIBLE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        Toast.makeText(LoginActivity.this, "Authentication Failed.", Toast.LENGTH_SHORT).show();
    }

    public void showView(){

    }
}

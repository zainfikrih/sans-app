package men.ngopi.zain.sans.controllers;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.features.checkout.CheckoutActivity;
import men.ngopi.zain.sans.features.main.order.OrdersFragment;
import men.ngopi.zain.sans.models.Order;
import men.ngopi.zain.sans.server.Server;

public class OrderController {

    private static OrderController orderController;
    private static Context context;
    private ArrayList<Order> process;
    private ArrayList<Order> complete;

    public static OrderController getInstance(Context cntx){
        if(orderController == null){
            orderController = new OrderController();
        }
        context = cntx;
        return orderController;
    }

    public void getOrderHistory(){
        Server.getInstance(context).allOrders();
    }

    public void onSuccessGetOrderHistory(ArrayList<Order> orders){
        process = new ArrayList<>();
        complete = new ArrayList<>();
        try {
            for(Order order: orders){
                if(order.getStatus().equalsIgnoreCase("process")){
                    process.add(order);
                }
                if(order.getStatus().equalsIgnoreCase("complete")){
                    complete.add(order);
                }
            }
        } catch (Exception e){

        } finally {
            OrdersFragment.getInstance().showOrderHistory(process, complete);
        }
    }

    public void createOrder(Order order){
        Server.getInstance(context).createOrder(order);
    }

    public void successCreateOrder(){
        CheckoutActivity.getInstance().successCreateOrder();
    }

    public void errorCreateOrder(){
        CheckoutActivity.getInstance().errorCreateOrder();
    }


}

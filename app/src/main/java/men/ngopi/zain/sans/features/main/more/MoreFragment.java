package men.ngopi.zain.sans.features.main.more;

import android.app.Activity;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.button.MaterialButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.firebase.auth.FirebaseAuth;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.AuthController;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.features.main.MainActivity;
import men.ngopi.zain.sans.features.login.LoginActivity;
import men.ngopi.zain.sans.features.recomendedrestaurant.RecomendedRestaurantActivity;
import men.ngopi.zain.sans.models.Customer;
import men.ngopi.zain.sans.network.Api;

public class MoreFragment extends Fragment {

    private View view;
    private FirebaseAuth firebaseAuth;
    static private MoreFragment moreFragment;
    private GoogleSignInClient mGoogleSignInClient;

    private TextView tvName, tvEmail, tvBalance;
    private ImageView ivProfile;
    private Toolbar toolbar;

    static public MoreFragment newInstance(){
        moreFragment = new MoreFragment();
        return moreFragment;
    }

    public static MoreFragment getInstance(){
        return moreFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_more, container, false);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Rak.initialize(getContext());
        firebaseAuth = FirebaseAuth.getInstance();

        tvEmail = view.findViewById(R.id.more_email);
        tvName = view.findViewById(R.id.more_name);
        tvBalance = view.findViewById(R.id.more_balance);
        ivProfile = view.findViewById(R.id.more_img);
        toolbar = view.findViewById(R.id.toolbar_more);
        ((AppCompatActivity)getActivity()).setSupportActionBar(toolbar);

        Log.i("ID TOKEN", Rak.grab(Api.ID_TOKEN, ""));

        Customer customer = Rak.grab("customer");

        tvEmail.setText(customer.getEmail());
        tvBalance.setText(String.valueOf(customer.getPayment().getBalance()));
        tvName.setText(customer.getName());
        String url = customer.getProfile_picture();
        Glide.with(getContext())
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_room_service)
                .into(ivProfile);

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken("537841684101-gcjou14igd5a1dar9qpufsqku8q16dmg.apps.googleusercontent.com")
                .requestEmail()
                .build();
        mGoogleSignInClient = GoogleSignIn.getClient(getContext(), gso);
        firebaseAuth = FirebaseAuth.getInstance();

        CustomerController.getInstance(getActivity()).getCustomer();

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.options_menu_more:
                // setup the alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Are you sure?");
                builder.setMessage("Logout...");

                // add the buttons
                builder.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        hideDialog(dialogInterface);
                    }
                });

                builder.setNegativeButton("Logout", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        AuthController.getInstance(getActivity()).logout(firebaseAuth, mGoogleSignInClient);
                    }
                });

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialogInterface) {
                        dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.colorRed));
                    }
                });
                dialog.show();

                break;
        }
        return false;
    }

    public void hideDialog(DialogInterface dialogInterface){
        dialogInterface.dismiss();
    }

    public void showLoginActivity(){
        Intent loginIntent = new Intent(MainActivity.getInstance(), LoginActivity.class);
        startActivity(loginIntent);
        MainActivity.getInstance().finish();
    }
}

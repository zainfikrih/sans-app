package men.ngopi.zain.sans.features.login;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.TaskExecutors;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GetTokenResult;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.features.main.MainActivity;
import men.ngopi.zain.sans.network.Api;

public class VerifyActivity extends AppCompatActivity {

    private String phoneNumber;
    private FirebaseAuth firebaseAuth;
    private TextInputEditText txtCodeVerify;
    private Button btnVerify;
    private String mVerificationId;
    private FirebaseUser mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify);
        Rak.initialize(getApplicationContext());
        AndroidNetworking.initialize(getApplicationContext());

        firebaseAuth = FirebaseAuth.getInstance();

        txtCodeVerify = findViewById(R.id.txtCodeVerify);
        btnVerify = findViewById(R.id.btnVerify);

        Intent intent = getIntent();
        phoneNumber = intent.getStringExtra("phoneNumber");
        sendVerificationCode(phoneNumber);
        firebaseAuth = FirebaseAuth.getInstance();

        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String code = txtCodeVerify.getText().toString().trim();
                if (code.isEmpty() || code.length() < 6) {
                    txtCodeVerify.setError("Enter valid code");
                    txtCodeVerify.requestFocus();
                    return;
                }

                //verifying the code entered manually
                verifyVerificationCode(code);
            }
        });
    }

    private void sendVerificationCode(String mobile) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                "+62" + mobile,
                60,
                TimeUnit.SECONDS,
                TaskExecutors.MAIN_THREAD,
                mCallbacks);
    }


    //the callback to detect the verification status
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        @Override
        public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {

            //Getting the code sent by SMS
            String code = phoneAuthCredential.getSmsCode();

            //sometime the code is not detected automatically
            //in this case the code will be null
            //so user has to manually enter the code
            if (code != null) {
                txtCodeVerify.setText(code);
                //verifying the code
                verifyVerificationCode(code);
            }
        }

        @Override
        public void onVerificationFailed(FirebaseException e) {
            Toast.makeText(VerifyActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
            super.onCodeSent(s, forceResendingToken);

            //storing the verification id that is sent to the user
            mVerificationId = s;
        }
    };

    private void verifyVerificationCode(String code) {
        //creating the credential
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(mVerificationId, code);

        //signing the user
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        firebaseAuth.signInWithCredential(credential)
                .addOnCompleteListener(VerifyActivity.this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //verification successful we will start the profile activity

                            mUser = firebaseAuth.getCurrentUser();
                            mUser.getIdToken(true).addOnCompleteListener(new OnCompleteListener<GetTokenResult>() {
                                @Override
                                public void onComplete(@NonNull Task<GetTokenResult> task) {
                                    String idToken = task.getResult().getToken();
                                    Log.i("ID TOKEN", idToken);

                                    //Query
                                    String query = "mutation ($idToken: String!) {"
                                            + " customerLogin(idToken: $idToken)"
                                            +"},";

                                    //Variables
                                    String variables = "";
                                    try {
                                        variables = new JSONObject()
                                                .put("idToken", idToken)
                                                .toString();
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                    try {
                                        AndroidNetworking.post(Api.BASE_URL)
                                                .addJSONObjectBody(new JSONObject().put("query", query).put("variables", variables))
                                                .setContentType(Api.CONTENT_TYPE)
                                                .setTag("login")
                                                .setPriority(Priority.MEDIUM)
                                                .build()
                                                .getAsJSONObject(new JSONObjectRequestListener() {
                                                    @Override
                                                    public void onResponse(JSONObject response) {
                                                        try {
                                                            Rak.entry(Api.ID_TOKEN, response.getJSONObject("data").getString("customerLogin"));
                                                            Log.i("Response", response.getJSONObject("data").getString("customerLogin"));

                                                            //Jika sukses masuk
                                                            Intent intent = new Intent(VerifyActivity.this, MainActivity.class);
                                                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                                            startActivity(intent);
                                                            LoginActivity.getInstance().finish();
                                                            finish();

                                                        } catch (JSONException e) {
                                                            e.printStackTrace();
                                                        }
                                                    }
                                                    @Override
                                                    public void onError(ANError anError) {
                                                        Log.i("Error Di Sini", anError.getErrorBody());
                                                    }
                                                });
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }
                                }
                            });

                        } else {

                            //verification unsuccessful.. display an error message

                            String message = "Somthing is wrong, we will fix it soon...";

                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                message = "Invalid code entered...";
                            }

                            Snackbar snackbar = Snackbar.make(findViewById(R.id.parent), message, Snackbar.LENGTH_LONG);
                            snackbar.setAction("Dismiss", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });
                            snackbar.show();
                        }
                    }
                });
    }
}

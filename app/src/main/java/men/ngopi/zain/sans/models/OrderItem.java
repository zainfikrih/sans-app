package men.ngopi.zain.sans.models;

public class OrderItem {
    public int menu_id;
    public int quantity;
    public String note;

    public OrderItem(){

    }

    public OrderItem(int menu_id, int quantity, String note) {
        this.menu_id = menu_id;
        this.quantity = quantity;
        this.note = note;
    }

    public int getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(int menu_id) {
        this.menu_id = menu_id;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}

package men.ngopi.zain.sans.models;

public class Customer {
    private String id;
    private String uid;
    private String email;
    private String name;
    private String profile_picture;
    private Payment payment;

    public Customer() {
    }

    public Customer(String id, String uid, String email, String name, String profile_picture, Payment payment) {
        this.id = id;
        this.uid = uid;
        this.email = email;
        this.name = name;
        this.profile_picture = profile_picture;
        this.payment = payment;
    }

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }
}

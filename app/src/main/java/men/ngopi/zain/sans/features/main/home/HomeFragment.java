package men.ngopi.zain.sans.features.main.home;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.support.design.card.MaterialCardView;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import java.time.Instant;
import java.util.ArrayList;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.controllers.RestaurantController;
import men.ngopi.zain.sans.features.main.MainActivity;
import men.ngopi.zain.sans.features.recomendedrestaurant.RecomendedRestaurantActivity;
import men.ngopi.zain.sans.models.Restaurant;

public class HomeFragment extends Fragment {

    private View view;
    static private HomeFragment homeFragment;

    private TextView tvRec1, tvRec2, tvNear1, tvNear2;
    private ImageView imgNear1 , imgNear2, imgRec1, imgRec2;
    private MaterialCardView cardView1, cardView2, cardView3, cardView4;

    private ArrayList<Restaurant> nearRrestaurants;
    private ArrayList<Restaurant> recomendedRrestaurants;
    private ArrayList<String> latlon = new ArrayList<>();

    private FusedLocationProviderClient fusedLocationClient;

    public static HomeFragment newInstance(){
        homeFragment = new HomeFragment();
        return homeFragment;
    }

    public static HomeFragment getInstance(){
        return homeFragment;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        tvRec1 = view.findViewById(R.id.home_rec_tv_1);
        tvRec2 = view.findViewById(R.id.home_rec_tv_2);
        tvNear1 = view.findViewById(R.id.home_near_tv_1);
        tvNear2 = view.findViewById(R.id.home_near_tv_2);
        imgRec1 = view.findViewById(R.id.home_rec_img_1);
        imgRec2 = view.findViewById(R.id.home_rec_img_2);
        imgNear1 = view.findViewById(R.id.home_near_img_1);
        imgNear2 = view.findViewById(R.id.home_near_img_2);
        cardView1 = view.findViewById(R.id.home_card_1);
        cardView2 = view.findViewById(R.id.home_card_2);
        cardView3 = view.findViewById(R.id.home_card_3);
        cardView4 = view.findViewById(R.id.home_card_4);



        cardView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    showActivityRestuarant(nearRrestaurants.get(0));
                } catch (Exception e){
                }
            }
        });

        cardView2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    showActivityRestuarant(nearRrestaurants.get(1));
                } catch (Exception e){
                }
            }
        });

        cardView3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    showActivityRestuarant(recomendedRrestaurants.get(0));
                } catch (Exception e){
                }
            }
        });

        cardView4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    showActivityRestuarant(recomendedRrestaurants.get(1));
                } catch (Exception e){
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();

        if(Rak.isExist("latlon")){
            latlon = Rak.grab("latlon");
            getRecAndNearResataurans();
        } else {
            latlon.add(0, String.valueOf(0));
            latlon.add(1, String.valueOf(0));
        }

        if(Rak.isExist("nearestRestaurant")){
            nearRrestaurants = Rak.grab("nearestRestaurant");
            showNearestRestaurant(nearRrestaurants);
        }

        if(Rak.isExist("recomendedRestaurants")){
            recomendedRrestaurants = Rak.grab("recomendedRestaurants");
            showRecomendedRestaurants(recomendedRrestaurants);
        }

        getLocation();
    }

    public void showActivityRestuarant(Restaurant restaurant){
        Intent intent = new Intent(getActivity(), RecomendedRestaurantActivity.class);
        Rak.entry("restaurantRecInfo", restaurant);
        startActivity(intent);
    }

    public void getLocation(){
        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(getActivity(), "Izinkan aplikasi untuk mengakses lokasi anda", Toast.LENGTH_SHORT).show();
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    1);
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location location) {
                        // Got last known location. In some rare situations this can be null.
                        if (location != null) {
                            // Logic to handle location object
                            latlon.set(0, String.valueOf(location.getLatitude()));
                            latlon.set(1, String.valueOf(location.getLongitude()));
                            Rak.entry("latlon", latlon);
                            getRecAndNearResataurans();
                        }
                    }
                });
    }

    public void getRecAndNearResataurans(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            RestaurantController.getInstance(getContext()).getNearestRestaurant(latlon.get(0), latlon.get(1));
            RestaurantController.getInstance(getContext()).getRecomendedRestaurants(latlon.get(0), latlon.get(1));
        }
        Log.i("LATLON", latlon.get(0) + ", " + latlon.get(1));
    }

    public void showNearestRestaurant(ArrayList<Restaurant> restaurant){
        try {
            nearRrestaurants = Rak.grab("nearestRestaurant");
            tvNear1.setText(restaurant.get(0).getName());
            tvNear2.setText(restaurant.get(1).getName());
            Glide.with(this)
                    .load("https://api.sans.ngopi.men"+restaurant.get(0).getPicture())
                    .centerCrop()
                    .placeholder(R.drawable.ic_room_service)
                    .into(imgNear1);

            Glide.with(this)
                    .load("https://api.sans.ngopi.men"+restaurant.get(1).getPicture())
                    .centerCrop()
                    .placeholder(R.drawable.ic_room_service)
                    .into(imgNear2);
        } catch (Exception e){

        }

    }

    public void showRecomendedRestaurants(ArrayList<Restaurant> restaurant){
        try {
            recomendedRrestaurants = Rak.grab("recomendedRestaurants");
            tvRec1.setText(restaurant.get(0).getName());
            tvRec2.setText(restaurant.get(1).getName());
            Glide.with(this)
                    .load("https://api.sans.ngopi.men"+restaurant.get(0).getPicture())
                    .centerCrop()
                    .placeholder(R.drawable.ic_room_service)
                    .into(imgRec1);

            Glide.with(this)
                    .load("https://api.sans.ngopi.men"+restaurant.get(1).getPicture())
                    .centerCrop()
                    .placeholder(R.drawable.ic_room_service)
                    .into(imgRec2);
        }catch (Exception e){

        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @androidx.annotation.NonNull String[] permissions, @androidx.annotation.NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode){
            case 1:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                    getLocation();
                } else {
                    Toast.makeText(getActivity(), "Permission denied to read your Location", Toast.LENGTH_SHORT).show();
                }
                return;
        }
    }
}

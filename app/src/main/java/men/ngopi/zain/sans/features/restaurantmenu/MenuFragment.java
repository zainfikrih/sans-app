package men.ngopi.zain.sans.features.restaurantmenu;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import java.util.ArrayList;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.features.restaurantmenu.adapters.MenuAdapter;
import men.ngopi.zain.sans.models.Menu;

public class MenuFragment extends Fragment {

    private View view;
    private RecyclerView recyclerView;

    public MenuFragment(){

    }

    public static MenuFragment newInstance(Bundle bundle){
        MenuFragment menuFragment = new MenuFragment();
        menuFragment.setArguments(bundle);
        return menuFragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.menu_recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        Menu[] menus = new Gson().fromJson(getArguments().getString("menus"), Menu[].class);
        ArrayList<Menu> menuList = new ArrayList<>();
        for (int i = 0; i < menus.length; i++){
            menuList.add(menus[i]);
        }

        MenuAdapter menuAdapter = new MenuAdapter(getActivity(), menuList);

        recyclerView.setAdapter(menuAdapter);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}

package men.ngopi.zain.sans.server;

import android.content.Context;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.controllers.AuthController;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.controllers.OrderController;
import men.ngopi.zain.sans.controllers.RestaurantController;
import men.ngopi.zain.sans.models.Category;
import men.ngopi.zain.sans.models.Customer;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.models.Order;
import men.ngopi.zain.sans.models.Restaurant;
import men.ngopi.zain.sans.network.Api;

public class Server {

    private static Server server;
    private static Context context;

    public static Server getInstance(Context cntx){
        if(server == null){
            server = new Server();
        }
        context = cntx;
        AndroidNetworking.initialize(context);
        return server;
    }

    public void customerLogin(String idToken){
        //Query
        String query = "mutation ($idToken: String!) {"
                + " customerLogin(idToken: $idToken)"
                +"},";

        //Variables
        String variables = "";
        try {
            variables = new JSONObject()
                    .put("idToken", idToken)
                    .toString();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", query).put("variables", variables))
                    .setContentType(Api.CONTENT_TYPE)
                    .setTag("login")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                Rak.entry(Api.ID_TOKEN, response.getJSONObject("data").getString("customerLogin"));
                                Log.i("Response", response.getJSONObject("data").getString("customerLogin"));

                                AuthController.getInstance(context).authSuccess();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                AuthController.getInstance(context).authFailed();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            Log.i("Error Di Sini", anError.getErrorBody());
                            AuthController.getInstance(context).authFailed();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
            AuthController.getInstance(context).authFailed();
        }
    }

    public void getCustomer(){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "query{ customer{ id, name, email, profile_picture, payment{ id, name, balance } }}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("getNameUser")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data").getJSONObject("customer");
                                Customer customer = new Gson().fromJson(jsonObject.toString(), Customer.class);
                                CustomerController.getInstance(context).showCustomer(customer);
                                Rak.entry("customer", customer);
                                //CustomerController.getInstance().showCustomer(response.getJSONObject("data").getJSONObject("customer"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void getCategories(){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "query{\n" +
                            "  allCategories(restaurant_id:1){\n" +
                            "    id,\n" +
                            "    name,\n" +
                            "    menus{\n" +
                            "      id,\n" +
                            "      name,\n" +
                            "      description,\n" +
                            "      price,\n" +
                            "      image\n" +
                            "    }\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("getCategories")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data").getJSONObject("allCategories");
                                Category category = new Gson().fromJson(jsonObject.toString(), Category.class);
//                                CustomerController.getInstance(context).showCustomer(customer);
                                //CustomerController.getInstance().showCustomer(response.getJSONObject("data").getJSONObject("customer"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void addFavoriteMenu(String menuId){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "mutation{\n" +
                            "  favoriteMenu(menu_id: "+ menuId +")\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("addFavoriteMenu")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data");
                                if(jsonObject.get("favoriteMenu").equals("OK")){
                                    CustomerController.getInstance(context).successAddFavoriteMenu();
                                }
//                                CustomerController.getInstance(context).showCustomer(customer);
                                //CustomerController.getInstance().showCustomer(response.getJSONObject("data").getJSONObject("customer"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void restaurant(String idRestaurant){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "\n" +
                            "query{\n" +
                            "  restaurant(id: "+ idRestaurant +"){\n" +
                            "    id,\n" +
                            "    name,\n" +
                            "    opening_time,\n" +
                            "    closing_time,\n" +
                            "    address,\n" +
                            "    picture,\n" +
                            "    categories {\n" +
                            "      id,\n" +
                            "      name,\n" +
                            "      menus {\n" +
                            "        id,\n" +
                            "        name,\n" +
                            "        description,\n" +
                            "        price,\n" +
                            "        image,\n" +
                            "        liked,\n" +
                            "      }\n" +
                            "    }\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("getRestaurant")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data").getJSONObject("restaurant");
                                Restaurant restaurant = new Gson().fromJson(jsonObject.toString(), Restaurant.class);
                                RestaurantController.getInstance(context).onRestaurantSuccess(restaurant);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void unFavoriteMenu(String menuId){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "mutation{\n" +
                            "  unfavoriteMenu(menu_id: "+ menuId +")\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("unFavoriteMenu")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data");
                                if(jsonObject.get("unfavoriteMenu").equals("OK")){
                                    CustomerController.getInstance(context).successAddFavoriteMenu();
                                }
//                                CustomerController.getInstance(context).showCustomer(customer);
                                //CustomerController.getInstance().showCustomer(response.getJSONObject("data").getJSONObject("customer"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void createOrder(Order order){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "mutation{\n" +
                            "  createOrder(restaurant_id: "+ order.getRestaurant().getId() +", order_items: "+ order.getOrder_items() +" , payment_method: OVO, customer_id: "+ order.getCustomer_id() +", table_number: "+ order.getTable_number() +"){\n" +
                            "    id\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("createOrder")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONObject jsonObject = response.getJSONObject("data");
                                Log.d("ID ORDER", jsonObject.getString("createOrder"));
                                OrderController.getInstance(context).successCreateOrder();
//                                CustomerController.getInstance(context).showCustomer(customer);
                                //CustomerController.getInstance().showCustomer(response.getJSONObject("data").getJSONObject("customer"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                                OrderController.getInstance(context).errorCreateOrder();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                            OrderController.getInstance(context).errorCreateOrder();
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void allOrders(){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "query{\n" +
                            "  allOrders{\n" +
                            "    id\n" +
                            "    total_price\n" +
                            "    table_number\n" +
                            "    status\n" +
                            "   \torder_number\n" +
                            "    restaurant{\n" +
                            "      name\n" +
                            "      address\n" +
                            "      picture\n" +
                            "    }\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("allOrders")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONObject("data").getJSONArray("allOrders");
                                Log.d("ORDER SERVER", jsonArray.toString());
                                Type listType = new TypeToken<ArrayList<Order>>() {}.getType();
                                ArrayList<Order> order = new Gson().fromJson(jsonArray.toString(), listType);
                                Rak.entry("orderHistory", order);
                                OrderController.getInstance(context).onSuccessGetOrderHistory(order);
//                                RestaurantController.getInstance(context).onRestaurantSuccess(restaurant);
//                                CustomerController.getInstance(context).showCustomer(customer);
                                //CustomerController.getInstance().showCustomer(response.getJSONObject("data").getJSONObject("customer"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void favoriteMenu(){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "\n" +
                            "query{\n" +
                            "  allFavoriteMenus{\n" +
                            "    id\n" +
                            "    name\n" +
                            "    description\n" +
                            "    price\n" +
                            "    image\n" +
                            "    liked\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("allFavoriteMenus")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONObject("data").getJSONArray("allFavoriteMenus");
                                Type listType = new TypeToken<ArrayList<Menu>>() {}.getType();
                                ArrayList<Menu> menus= new Gson().fromJson(jsonArray.toString(), listType);
                                Rak.entry("favoriteMenus", menus);
                                Log.d("ISI ORDER STR", jsonArray.toString());
                                CustomerController.getInstance(context).successGetFavoriteMenu(menus);
//                                RestaurantController.getInstance(context).onRestaurantSuccess(restaurant);
//                                CustomerController.getInstance(context).showCustomer(customer);
                                //CustomerController.getInstance().showCustomer(response.getJSONObject("data").getJSONObject("customer"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void nearestRestaurant(String lat, String lon){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "query{\n" +
                            "  nearbyRestaurants(lat: \""+ lat +"\", long: \""+ lon +"\"){\n" +
                            "    id,\n" +
                            "    name,\n" +
                            "    opening_time,\n" +
                            "    closing_time,\n" +
                            "    address,\n" +
                            "    picture,\n" +
                            "    categories {\n" +
                            "      id,\n" +
                            "      name,\n" +
                            "      menus {\n" +
                            "        id,\n" +
                            "        name,\n" +
                            "        description,\n" +
                            "        price,\n" +
                            "        image,\n" +
                            "        liked,\n" +
                            "      }\n" +
                            "    }\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("nearestRestaurant")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONObject("data").getJSONArray("nearbyRestaurants");
                                Type listType = new TypeToken<ArrayList<Restaurant>>() {}.getType();
                                ArrayList<Restaurant> restaurants= new Gson().fromJson(jsonArray.toString(), listType);
                                Log.d("ISI ORDER STR Near", jsonArray.toString());
                                Rak.entry("nearestRestaurant", restaurants);
                                RestaurantController.getInstance(context).successGetNearestRestaurant(restaurants);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI Near", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void recomendedRestaurants(String lat, String lon){
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "query{\n" +
                            "  recomendedRestaurants(lat: \""+ lat +"\", long: \""+ lon +"\"){\n" +
                            "    id,\n" +
                            "    name,\n" +
                            "    opening_time,\n" +
                            "    closing_time,\n" +
                            "    address,\n" +
                            "    picture,\n" +
                            "    categories {\n" +
                            "      id,\n" +
                            "      name,\n" +
                            "      menus {\n" +
                            "        id,\n" +
                            "        name,\n" +
                            "        description,\n" +
                            "        price,\n" +
                            "        image,\n" +
                            "        liked,\n" +
                            "      }\n" +
                            "    }\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ Rak.grab(Api.ID_TOKEN))
                    .setTag("recomendedRestaurants")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                JSONArray jsonArray = response.getJSONObject("data").getJSONArray("recomendedRestaurants");
                                Type listType = new TypeToken<ArrayList<Restaurant>>() {}.getType();
                                ArrayList<Restaurant> restaurants= new Gson().fromJson(jsonArray.toString(), listType);
                                Log.d("ISI ORDER STR Rec", jsonArray.toString());
                                Rak.entry("recomendedRestaurants", restaurants);
                                RestaurantController.getInstance(context).successGetRecomendedRestaurants(restaurants);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR DI SINI Rec", anError.getErrorBody());
                        }
                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}

package men.ngopi.zain.sans.features.restaurantmenu.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.button.MaterialButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.features.restaurantmenu.RestaurantActivity;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.models.OrderItem;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MenuViewHolder> {

    private Context context;
    private ArrayList<Menu> menu;
    private LayoutInflater layoutInflater;
    private View view;

    private ArrayList<OrderItem> orderItems = new ArrayList<>();
    private HashMap<String, OrderItem> orderItemHashMap = new HashMap<>();
    private HashMap<String, Menu> menuHashMap = new HashMap<>();

    public MenuAdapter (Context context, ArrayList<Menu> menu){
        this.context = context;
        this.menu = menu;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public MenuAdapter.MenuViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = layoutInflater.inflate(R.layout.menu_item, viewGroup, false);
        return new MenuAdapter.MenuViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuAdapter.MenuViewHolder menuViewHolder, final int i) {
        final Menu menuItem = menu.get(i);
        menuViewHolder.tvName.setText(menuItem.getName());
        menuViewHolder.tvDesc.setText(menuItem.getDescription());
        menuViewHolder.tvPrice.setText(String.valueOf("Rp. "+menuItem.getPrice()));
        menuViewHolder.tvTotal.setText("0");
        if(menuItem.getLiked()){
            menuViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_true));
        }

        String url = "https://api.sans.ngopi.men"+menuItem.getImage();

        Glide.with(context)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_room_service)
                .into(menuViewHolder.ivMenu);

        menuViewHolder.btnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Rak.isExist("orderItems")){
                    orderItemHashMap = Rak.grab("orderItems");
                    menuHashMap = Rak.grab("menuItems");
                }
                OrderItem orderItem = new OrderItem();
                orderItem.setMenu_id(Integer.parseInt(menuItem.getId()));
                orderItem.setQuantity(0);
                orderItem.setNote("");
                int total = Integer.parseInt(menuViewHolder.tvTotal.getText().toString());
                if(total==0){
                    // tambah ke list
//                    orderItems.set(i, new OrderItem());
//                    orderItems.add(i, orderItem);

                    menuHashMap.put(String.valueOf(orderItem.getMenu_id()), menuItem);
                    orderItemHashMap.put(String.valueOf(orderItem.getMenu_id()), orderItem);
                    menuViewHolder.ivNote.setVisibility(View.VISIBLE);
                    menuViewHolder.btnMin.setVisibility(View.VISIBLE);
                }

                menuViewHolder.tvTotal.setText(String.valueOf(++total));

                if(total>0){
                    // update quantity
//                    orderItems.get(i).setQuantity(total);
                    orderItemHashMap.get(String.valueOf(orderItem.getMenu_id())).setQuantity(total);
                }

                orderItems.clear();
                orderItems.addAll(orderItemHashMap.values());
                Rak.entry("orderItems", orderItemHashMap);
                Rak.entry("menuItems", menuHashMap);
                String order = new Gson().toJson(orderItems);
                Log.d("Order items", order);

                RestaurantActivity.getInstance().plusBottomSheet(menuItem.getPrice());
            }
        });

        menuViewHolder.btnMin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(Rak.isExist("orderItems")){
                    orderItemHashMap = Rak.grab("orderItems");
                    menuHashMap = Rak.grab("menuItems");
                }
                int total = Integer.parseInt(menuViewHolder.tvTotal.getText().toString());
                if(total != 0){
                    menuViewHolder.tvTotal.setText(String.valueOf(--total));
                }
                if(total > 0){
                    //update quantity
//                    orderItems.get(i).setQuantity(total);
                    orderItemHashMap.get(String.valueOf(menuItem.getId())).setQuantity(total);
                    RestaurantActivity.getInstance().minusBottomSheet(menuItem.getPrice());
                } if(total == 0) {
                    // hapus dari list
                    try {
//                        orderItems.remove(i);
                        menuHashMap.remove(menuItem.getId());
                        orderItemHashMap.remove(menuItem.getId());
                        menuViewHolder.ivNote.setColorFilter(ContextCompat.getColor(context, R.color.colorGrayBtn), android.graphics.PorterDuff.Mode.MULTIPLY);
                        menuViewHolder.ivNote.setVisibility(View.GONE);
                        RestaurantActivity.getInstance().minusBottomSheet(menuItem.getPrice());
                        menuViewHolder.btnMin.setVisibility(View.GONE);
//                        RestaurantActivity.getInstance().hideBottomSheet();
                    } catch (Exception e){

                    }
                }

                orderItems.clear();
                orderItems.addAll(orderItemHashMap.values());
                Rak.entry("orderItems", orderItemHashMap);
                Rak.entry("menuItems", menuHashMap);
                String order = new Gson().toJson(orderItems);
                Log.d("Order items", order);
            }
        });

        menuViewHolder.ivNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LayoutInflater inflater = LayoutInflater.from(context);
                View dialogView = inflater.inflate(R.layout.dialog_note, null);

                final EditText editText = dialogView.findViewById(R.id.dn_note);

                editText.setText(orderItemHashMap.get(menuItem.getId()).getNote());

                // setup the alert builder
//                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
                AlertDialog.Builder builder = new AlertDialog.Builder(context, R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Add Note");

                // add the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        orderItemHashMap.get(menuItem.getId()).setNote(editText.getText().toString());

                        if(editText.getText().length() > 0){
                            menuViewHolder.ivNote.setColorFilter(ContextCompat.getColor(context, R.color.colorGreen), android.graphics.PorterDuff.Mode.MULTIPLY);
                        } else {
                            menuViewHolder.ivNote.setColorFilter(ContextCompat.getColor(context, R.color.colorGrayBtn), android.graphics.PorterDuff.Mode.MULTIPLY);
                        }

                        orderItems.clear();
                        orderItems.addAll(orderItemHashMap.values());
                        Rak.entry("orderItems", orderItemHashMap);
                        String order = new Gson().toJson(orderItems);
                        Log.d("Order items", order);
                    }
                });

                builder.setView(dialogView);

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();
                Window window = dialog.getWindow();
                window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

            }
        });

        menuViewHolder.ivFav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(menuItem.getLiked()){
                    CustomerController.getInstance(context.getApplicationContext()).unFavoriteMenu(String.valueOf(menuItem.getId()));
                    menuViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite));
                    menuViewHolder.ivFav.setColorFilter(ContextCompat.getColor(context, R.color.colorGrayBtn), android.graphics.PorterDuff.Mode.MULTIPLY);
                    menuItem.setLiked(false);
                } else {
                    CustomerController.getInstance(context.getApplicationContext()).addFavoriteMenu(String.valueOf(menuItem.getId()));
                    menuViewHolder.ivFav.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_favorite_true));
                    menuItem.setLiked(true);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return menu.size();
    }

    public class MenuViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvDesc;
        private TextView tvPrice;
        private TextView tvTotal;
        private MaterialButton btnPlus;
        private MaterialButton btnMin;
        private ImageView ivMenu;
        private ImageView ivFav;
        private ImageView ivNote;

        public MenuViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.menu_tittle);
            tvDesc = itemView.findViewById(R.id.menu_desc);
            tvPrice = itemView.findViewById(R.id.menu_price);
            tvTotal = itemView.findViewById(R.id.menu_total);
            btnPlus = itemView.findViewById(R.id.menu_btn_plus);
            btnMin = itemView.findViewById(R.id.menu_btn_minus);
            ivMenu = itemView.findViewById(R.id.menu_img);
            ivFav = itemView.findViewById(R.id.menu_fav);
            ivNote = itemView.findViewById(R.id.menu_note);

        }
    }
}

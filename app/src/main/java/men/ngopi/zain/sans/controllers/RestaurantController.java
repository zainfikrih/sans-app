package men.ngopi.zain.sans.controllers;

import android.content.Context;

import java.util.ArrayList;

import men.ngopi.zain.sans.features.main.home.HomeFragment;
import men.ngopi.zain.sans.features.restaurantmenu.RestaurantActivity;
import men.ngopi.zain.sans.models.Restaurant;
import men.ngopi.zain.sans.server.Server;

public class RestaurantController {

    private static RestaurantController restaurantController;
    private static Context context;

    public static RestaurantController getInstance(Context cntx){
        if(restaurantController == null){
            restaurantController = new RestaurantController();
        }
        context = cntx;
        return restaurantController;
    }

    public void getRestaurant(String idRestaurant){
        Server.getInstance(context).restaurant(idRestaurant);
    }

    public void onRestaurantSuccess(Restaurant restaurant){
        RestaurantActivity.getInstance().onRestaurantSuccess(restaurant);
    }

    public void getNearestRestaurant(String lat, String lon){
        Server.getInstance(context).nearestRestaurant(lat, lon);
    }

    public void successGetNearestRestaurant(ArrayList<Restaurant> restaurant){
        HomeFragment.getInstance().showNearestRestaurant(restaurant);
    }

    public void getRecomendedRestaurants(String lat, String lon){
        Server.getInstance(context).recomendedRestaurants(lat, lon);
    }

    public void successGetRecomendedRestaurants(ArrayList<Restaurant> restaurants){
        HomeFragment.getInstance().showRecomendedRestaurants(restaurants);
    }
}

package men.ngopi.zain.sans.features.restaurantmenu;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.button.MaterialButton;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ogaclejapan.smarttablayout.SmartTabLayout;
import com.ogaclejapan.smarttablayout.utils.v4.FragmentPagerItemAdapter;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import androidx.annotation.RequiresApi;
import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.controllers.RestaurantController;
import men.ngopi.zain.sans.features.checkout.CheckoutActivity;
import men.ngopi.zain.sans.models.Category;
import men.ngopi.zain.sans.models.Restaurant;
import men.ngopi.zain.sans.utils.Util;

public class RestaurantActivity extends AppCompatActivity {

    private FragmentPagerItemAdapter adapter;
    private static RestaurantActivity restaurantActivity;

    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ImageView imageView;

    private View layoutBottomSheet;
    private BottomSheetBehavior sheetBehavior;
    private TextView tvBsPrice;
    private Button btnBsCheckout;

    private TextView tvLocation, tvTime, tvTable;
    private int position = 0;

    private MenuFragmentAdapter adapterPage;
    private List<String> tabItems = new ArrayList<>();
    private Bundle bundle;
    private ViewPager viewPager;
    private SmartTabLayout viewPagerTab;
    private ArrayList<ArrayList<String>> list;

    private Restaurant restaurant;
    private Restaurant restaurantInfo;
    private String idRestaurant;
    private String openingTime;
    private String closingTime;

    public static RestaurantActivity getInstance(){
        return restaurantActivity;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
        Rak.initialize(getApplicationContext());

        restaurantActivity = this;

        restaurantInfo = Rak.grab("restaurant");
        idRestaurant = Rak.grab("idRestaurant");

        collapsingToolbarLayout = findViewById(R.id.restaurant_collapsing_toolbar);
        imageView = findViewById(R.id.restaurant_img_toolbar);
        toolbar = findViewById(R.id.restaurant_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(restaurantInfo.getName());
        getSupportActionBar().setSubtitle(restaurantInfo.getAddress());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        Drawable drawable = getDrawable(R.drawable.ic_info);
        toolbar.setOverflowIcon(drawable);

        openingTime = Util.formatDate(restaurantInfo.getOpening_time());
        closingTime = Util.formatDate(restaurantInfo.getClosing_time());

        String url = "https://api.sans.ngopi.men"+restaurantInfo.getPicture();

        Glide.with(this)
                .load(url)
                .centerCrop()
                .placeholder(R.drawable.ic_room_service)
                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                        imageView.setImageDrawable(resource);

                        Bitmap bm = ((BitmapDrawable)imageView.getDrawable()).getBitmap();
                        Palette.from(bm).generate(new Palette.PaletteAsyncListener() {
                            @Override
                            public void onGenerated(Palette palette) {
                                int mutedColor = palette.getDarkMutedColor(R.attr.colorPrimary); //Muted, Dark, Light, Vibrant
                                collapsingToolbarLayout.setContentScrimColor(mutedColor);

                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    Window window = getWindow();
                                    window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                                    window.setStatusBarColor(mutedColor);
                                    int flags = window.getDecorView().getSystemUiVisibility();
                                    flags &= ~View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
                                    window.getDecorView().setSystemUiVisibility(flags);
                                }
                            }
                        });
                    }
                });

        RestaurantController.getInstance(getApplicationContext()).getRestaurant(idRestaurant);

        layoutBottomSheet = findViewById(R.id.bottom_sheet);
        tvBsPrice = findViewById(R.id.bs_price);
        btnBsCheckout = findViewById(R.id.bs_btn_checkout);

        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);

        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED:
                        break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });

        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        btnBsCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RestaurantActivity.this, CheckoutActivity.class);
                intent.putExtra("calculatedPrice", tvBsPrice.getText().toString());
                startActivity(intent);
            }
        });

    }

    public void plusBottomSheet(int price){
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        tvBsPrice.setText(String.valueOf(Integer.valueOf(tvBsPrice.getText().toString())+price));
    }

    public void minusBottomSheet(int price){
        if (sheetBehavior.getState() != BottomSheetBehavior.STATE_COLLAPSED) {
            sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
        tvBsPrice.setText(String.valueOf(Integer.valueOf(tvBsPrice.getText().toString())-price));
        if(Integer.valueOf(tvBsPrice.getText().toString()) <= 0){
            hideBottomSheet();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        CustomerController.getInstance(getApplicationContext()).getCustomer();
    }

    public void hideBottomSheet(){
        sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu_restoran, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.options_menu_restoran:
                // setup the alert builder
                AlertDialog.Builder builder = new AlertDialog.Builder(RestaurantActivity.this, R.style.AppCompatAlertDialogStyle);
                LayoutInflater inflater = this.getLayoutInflater();
                View dialogView = inflater.inflate(R.layout.dialog_restaurant, null);

                tvLocation = dialogView.findViewById(R.id.dialog_restaurant_location);
                tvTime = dialogView.findViewById(R.id.dialog_restaurant_time);
                tvTable = dialogView.findViewById(R.id.dialog_restaurant_table);

                tvLocation.setText(restaurantInfo.getAddress());
                tvTable.setText(Rak.grab("table").toString());
                String time = openingTime + " - " + closingTime;
                tvTime.setText(time);

                builder.setView(dialogView);
                builder.setTitle(restaurantInfo.getName());

                // add the buttons
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                // create and show the alert dialog
                AlertDialog dialog = builder.create();
                dialog.show();

                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return super.onSupportNavigateUp();
    }

    private void addTab(String title) {
//        viewPagerTab.addTab(viewPagerTab.newTab().setText(title));
        addTabPage(title);
    }

    public void addTabPage(String title) {
        tabItems.add(title);
        adapterPage.notifyDataSetChanged();
    }

    public void onRestaurantSuccess(Restaurant restaurant){
        this.restaurant = restaurant;
        Toast.makeText(RestaurantActivity.this, restaurant.getName(), Toast.LENGTH_SHORT).show();

        List<Category> categories = restaurant.getCategories();

        List<String> tabName = new ArrayList<>();
        for (Category category: categories) {
            tabName.add(category.getName());
        }

        viewPager = (ViewPager) findViewById(R.id.restaurant_vp);
        viewPager.setOffscreenPageLimit(10);
        viewPagerTab = (SmartTabLayout) findViewById(R.id.restaurant_tab);

        adapterPage = new MenuFragmentAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapterPage);

        for(int i = 0; i < tabName.size(); i++){
            addTab(tabName.get(i));
        }

        viewPagerTab.setViewPager(viewPager);
    }

    private class MenuFragmentAdapter extends FragmentPagerAdapter{

        public MenuFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int i) {
            bundle = new Bundle();
            Gson gson = new GsonBuilder().create();
            String menus = gson.toJson(restaurant.getCategories().get(i).getMenus());
            bundle.putString("menus", menus);
            return MenuFragment.newInstance(bundle);
        }

        @Override
        public int getCount() {
            return tabItems.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return tabItems.get(position);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Rak.remove("orderItems");
        Rak.remove("menuItems");
    }
}

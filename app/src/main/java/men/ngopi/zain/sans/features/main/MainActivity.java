package men.ngopi.zain.sans.features.main;

import android.Manifest;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Transition;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONObject;

import java.util.ArrayList;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.features.main.favorite.FavoriteFragment;
import men.ngopi.zain.sans.features.main.home.HomeFragment;
import men.ngopi.zain.sans.features.main.more.MoreFragment;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.features.main.order.OrdersFragment;
import men.ngopi.zain.sans.features.scanqrcode.ScanActivity;
import men.ngopi.zain.sans.features.splashscreen.SplashActivity;
import okhttp3.Credentials;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import spencerstudios.com.bungeelib.Bungee;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth firebaseAuth;
    private FirebaseUser firebaseUser;
    private BottomNavigationView bottomNavigationView;
    private FloatingActionButton floatingActionButton;
    private Fragment selectedFragment;
    private android.support.v4.app.Fragment selectedFragmentV4;
    static private MainActivity mainActivity;
    private FragmentTransaction fragmentTransaction;
    private android.support.v4.app.FragmentTransaction fragmentTransactionV4;

    public static MainActivity getInstance() {
        return mainActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AndroidNetworking.initialize(getApplicationContext());
        Rak.initialize(getApplicationContext());

        mainActivity = this;

        CustomerController.getInstance(getApplicationContext()).getCustomer();

        firebaseAuth = FirebaseAuth.getInstance();
        firebaseUser = firebaseAuth.getCurrentUser();
        bottomNavigationView = findViewById(R.id.navigation);
        floatingActionButton = findViewById(R.id.floatingActionButton);

        if(Rak.isExist("orderItems")){
            Rak.remove("orderItems");
            Rak.remove("menuItems");
        }

        if(firebaseAuth.getCurrentUser() == null){
            Intent splashIntent = new Intent(this, SplashActivity.class);
            startActivity(splashIntent);
            finish();
        }

        selectedFragmentV4 = OrdersFragment.newInstance();

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransactionV4 = getSupportFragmentManager().beginTransaction();
                switch (menuItem.getItemId()){
                    case R.id.action_item1:
                        selectedFragment = HomeFragment.newInstance();
                        if(!fragmentTransactionV4.isEmpty()){
                            fragmentTransactionV4.detach(selectedFragmentV4);
                            fragmentTransactionV4.hide(selectedFragmentV4);
                            fragmentTransactionV4.commit();
                        }

                        fragmentTransaction.replace(R.id.content, selectedFragment);
                        fragmentTransaction.commit();
                        break;
                    case R.id.action_item2:
                        selectedFragmentV4 = OrdersFragment.newInstance();
                        fragmentTransaction.detach(selectedFragment);
                        fragmentTransaction.hide(selectedFragment);
                        fragmentTransaction.remove(selectedFragment);
                        fragmentTransaction.commit();

                        fragmentTransactionV4.replace(R.id.content, selectedFragmentV4);
                        fragmentTransactionV4.commit();
                        break;
                    case R.id.action_item3:
                        return false;
                    case R.id.action_item4:
                        selectedFragmentV4 = FavoriteFragment.newInstance();
                        fragmentTransaction.detach(selectedFragment);
                        fragmentTransaction.hide(selectedFragment);
                        fragmentTransaction.remove(selectedFragment);
                        fragmentTransaction.commit();

                        fragmentTransactionV4.replace(R.id.content, selectedFragmentV4);
                        fragmentTransactionV4.commit();
                        break;
                    case R.id.action_item5:
                        selectedFragment = MoreFragment.newInstance();
                        if(!fragmentTransactionV4.isEmpty()){
                            fragmentTransactionV4.detach(selectedFragmentV4);
                            fragmentTransactionV4.hide(selectedFragmentV4);
                            fragmentTransactionV4.commit();
                        }

                        fragmentTransaction.replace(R.id.content, selectedFragment);
                        fragmentTransaction.commit();
                        break;
                }

                return true;
            }
        });

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ScanActivity.class);
                startActivity(intent);
                Bungee.slideUp(MainActivity.this);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu_more, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        super.onStart();
        bottomNavigationView.setSelectedItemId(R.id.action_item1);
        CustomerController.getInstance(getApplicationContext()).getCustomer();
    }
}

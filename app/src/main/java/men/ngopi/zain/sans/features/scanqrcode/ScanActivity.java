package men.ngopi.zain.sans.features.scanqrcode;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.budiyev.android.codescanner.AutoFocusMode;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.budiyev.android.codescanner.ScanMode;
import com.google.gson.Gson;
import com.google.zxing.Result;
import com.victor.loading.rotate.RotateLoading;

import org.json.JSONException;
import org.json.JSONObject;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.features.restaurantmenu.RestaurantActivity;
import men.ngopi.zain.sans.models.Restaurant;
import men.ngopi.zain.sans.network.Api;
import spencerstudios.com.bungeelib.Bungee;

public class ScanActivity extends AppCompatActivity {

    private CodeScanner codeScanner;
    private Toolbar toolbar;
    private String[] dataStr;
    private String idResto, idToken;
    private RotateLoading rotateLoading;
    private LinearLayout linearLayout;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        //FAN
        AndroidNetworking.initialize(getApplicationContext());

        //Rak
        Rak.initialize(getApplicationContext());

        //ID TOKEN
        idToken = Rak.grab(Api.ID_TOKEN);

        //RotateLoading
        rotateLoading = findViewById(R.id.rotateloading);
        linearLayout = findViewById(R.id.linear_layout_loading);

        //Toolbar
        toolbar = findViewById(R.id.toolbar_scan);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Scan QR Code");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //CodeScanner
        CodeScannerView codeScannerView = findViewById(R.id.scanner_view);
        codeScanner = new CodeScanner(this, codeScannerView);
        codeScanner.setAutoFocusMode(AutoFocusMode.CONTINUOUS);
        codeScanner.setScanMode(ScanMode.SINGLE);
        codeScanner.setFlashEnabled(false);
        codeScanner.setTouchFocusEnabled(false);
        codeScannerView.setFlashButtonVisible(false);
        codeScannerView.setAutoFocusButtonVisible(false);
        codeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            byte[] data = new byte[0];
                            data = Base64.decode(result.getText().getBytes(), Base64.DEFAULT);
                            dataStr = new String(data).split("@");
                            idResto = dataStr[0];
                            Rak.entry("idRestaurant", idResto);
                            Rak.entry("table", dataStr[1]);
                            Toast.makeText(ScanActivity.this, dataStr[0], Toast.LENGTH_SHORT).show();
                        } catch (Exception e){
                            Toast.makeText(ScanActivity.this, "QR Code Invalid", Toast.LENGTH_SHORT).show();
                            finish();
                        }
                        getResturant();
                    }
                });
            }
        });

        //Click CodeScanner
        codeScannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startPrev();
            }
        });

    }

    //Start RestaurantActivity
    private void startRestaurantActivity(){
        Intent restaurantIntent = new Intent(ScanActivity.this, RestaurantActivity.class);
        startActivity(restaurantIntent);
        finish();
    }

    //Get Restaurant
    private void getResturant(){

        //Show Loading
        linearLayout.setVisibility(View.VISIBLE);
        rotateLoading.start();

        //Get
        try {
            AndroidNetworking.post(Api.BASE_URL)
                    .addJSONObjectBody(new JSONObject().put("query", "query{\n" +
                            "restaurant(id: "+ idResto +"){\n" +
                            "    id,\n" +
                            "    name,\n" +
                            "    opening_time,\n" +
                            "    closing_time,\n" +
                            "    address,\n" +
                            "    picture,\n" +
                            "  }\n" +
                            "}"))
                    .setContentType(Api.CONTENT_TYPE)
                    .addHeaders("Authorization", "Bearer "+ idToken)
                    .setTag("getRestaurant")
                    .setPriority(Priority.MEDIUM)
                    .build()
                    .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                        public void onResponse(JSONObject response) {
                            try {
                                //txtTesToken.setText(response.getJSONObject("data").getJSONObject("customer").getString("name"));
                                JSONObject jsonObject = response.getJSONObject("data").getJSONObject("restaurant");
                                Log.i("Restaurant Info", response.getJSONObject("data").toString());
                                Restaurant restaurant = new Gson().fromJson(jsonObject.toString(), Restaurant.class);

                                Rak.entry("restaurant", restaurant);

                                startRestaurantActivity();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        @Override
                        public void onError(ANError anError) {
                            Log.i("ERROR | Get Restaurant", anError.getMessage());
                            finish();
                        }

                    });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //Start Code Scanner
    private void startPrev(){
        codeScanner.startPreview();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        Bungee.slideDown(ScanActivity.this);
        return super.onSupportNavigateUp();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Bungee.slideDown(ScanActivity.this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (ContextCompat.checkSelfPermission(ScanActivity.this, Manifest.permission.CAMERA)
                == PackageManager.PERMISSION_DENIED){
            ActivityCompat.requestPermissions(ScanActivity.this, new String[] {Manifest.permission.CAMERA}, 111);
        } else {
            startPrev();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @android.support.annotation.NonNull String[] permissions, @android.support.annotation.NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 111){
            if(grantResults[0] == PackageManager.PERMISSION_GRANTED){
                startPrev();
            } else {
                Toast.makeText(ScanActivity.this, "Camera permission denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        codeScanner.releaseResources();
    }


}

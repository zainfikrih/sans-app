package men.ngopi.zain.sans.features.checkout.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.design.button.MaterialButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Objects;

import io.isfaaghyth.rak.Rak;
import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.controllers.CustomerController;
import men.ngopi.zain.sans.features.restaurantmenu.RestaurantActivity;
import men.ngopi.zain.sans.models.Menu;
import men.ngopi.zain.sans.models.OrderItem;

public class OrderItemAdapter extends RecyclerView.Adapter<OrderItemAdapter.OrderItemViewHolder> {

    private Context context;
    private ArrayList<OrderItem> orderItems;
    private ArrayList<Menu> menuItems;
    private HashMap<String, Menu> menuHashMap = new HashMap<>();
    private LayoutInflater layoutInflater;
    private View view;
    private String name, price;

    public OrderItemAdapter(Context context, ArrayList<OrderItem> orderItems, HashMap<String, Menu> menuHashMap){
        this.context = context;
        this.orderItems = orderItems;
        this.menuHashMap = menuHashMap;
        layoutInflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public OrderItemAdapter.OrderItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        view = layoutInflater.inflate(R.layout.order_item, viewGroup, false);
        return new OrderItemAdapter.OrderItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final OrderItemAdapter.OrderItemViewHolder orderItemViewHolder, final int i) {
        OrderItem orderItem = orderItems.get(i);
        Log.d("ID ORDER ITEM", String.valueOf(orderItem.getMenu_id()));
        try {
            name = String.valueOf(menuHashMap.get(String.valueOf(orderItem.getMenu_id())).getName());
            price = String.valueOf("Rp. " + menuHashMap.get(String.valueOf(orderItem.getMenu_id())).getPrice());

        } catch (Exception e){

        }
//        for (Menu menu: menuItems) {
//            if(menu.getId().equalsIgnoreCase(orderItem.getMenu_id())){
//                orderItemViewHolder.tvName.setText(String.valueOf(menu.getName()));
//                orderItemViewHolder.tvPrice.setText(String.valueOf("Rp. " + menu.getPrice()));
//            }
//        }
//        Menu menu = menuHashMap.get(String.valueOf(orderItem.getMenu_id()));
        orderItemViewHolder.tvName.setText(name);
        orderItemViewHolder.tvPrice.setText(price);
        orderItemViewHolder.tvQuantity.setText(String.valueOf(orderItem.getQuantity() + "x"));
        orderItemViewHolder.tvNote.setText(String.valueOf(orderItem.getNote()));
    }

    @Override
    public int getItemCount() {
        return orderItems.size();
    }

    public class OrderItemViewHolder extends RecyclerView.ViewHolder {

        private TextView tvName;
        private TextView tvNote;
        private TextView tvQuantity;
        private TextView tvPrice;

        public OrderItemViewHolder(@NonNull View itemView) {
            super(itemView);

            tvName = itemView.findViewById(R.id.orderItem_name);
            tvNote = itemView.findViewById(R.id.orderItem_note);
            tvQuantity = itemView.findViewById(R.id.orderItem_quantity);
            tvPrice = itemView.findViewById(R.id.orderItem_price);

        }
    }
}

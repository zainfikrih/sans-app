package men.ngopi.zain.sans.features.recomendedrestaurant;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;

import com.google.gson.Gson;

import java.util.ArrayList;

import men.ngopi.zain.sans.R;
import men.ngopi.zain.sans.features.recomendedrestaurant.adapters.RecMenuAdapter;
import men.ngopi.zain.sans.features.restaurantmenu.adapters.MenuAdapter;
import men.ngopi.zain.sans.models.Menu;

public class RecMenuFragment extends Fragment {

    private View view;
    private RecyclerView recyclerView;

    public RecMenuFragment(){

    }

    public static RecMenuFragment newInstance(Bundle bundle){
        RecMenuFragment recMenuFragment= new RecMenuFragment();
        recMenuFragment.setArguments(bundle);
        return recMenuFragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_menu, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @android.support.annotation.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.menu_recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setHasFixedSize(true);

        Menu[] menus = new Gson().fromJson(getArguments().getString("menus"), Menu[].class);
        ArrayList<Menu> menuList = new ArrayList<>();
        for (int i = 0; i < menus.length; i++){
            menuList.add(menus[i]);
        }

        RecMenuAdapter recMenuAdapter = new RecMenuAdapter(getActivity(), menuList);

        recyclerView.setAdapter(recMenuAdapter);

    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }
}
